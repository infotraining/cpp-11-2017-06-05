#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <cassert>
#include <memory>

using namespace std;

class Gadget
{
    int id_ = -1;
    string name_ = "unknown";
public:
    Gadget() = default;

    Gadget(int id) : id_{id}
    {
        cout << "Gagdet(" << id_ << ")" << endl;
    }

    Gadget(int id, const string& name) : id_{id}, name_{name}
    {
        cout << "Gagdet(" << id_ << ", " << name_ << ")" << endl;
    }

    virtual ~Gadget()
    {
        cout << "~Gadget(" << id_ << ")" << endl;
    }

    int id() const
    {
        return id_;
    }

    virtual void use()
    {
        cout << "Using Gadget(" << id_ << ")\n";
    }
};

class SuperGadget : public Gadget
{
public:
    using Gadget::Gadget;

    void use() override
    {
        cout << "##########\n";
        Gadget::use();
    }
};


// source - factory
auto_ptr<Gadget> create_gadget()
{
    static int counter = 0;
    return auto_ptr<Gadget>(new Gadget(++counter));
}

void use(Gadget* g)
{
    if (g)
        g->use();
}

// sink
auto_ptr<Gadget> use(auto_ptr<Gadget> ptr)
{
    ptr->use();

    return ptr;
}

TEST_CASE("auto_ptr")
{
    SECTION("controls lifetime of dynamic object")
    {
        auto_ptr<Gadget> aptr1 = create_gadget();
        auto_ptr<Gadget> aptr2 = aptr1; // cc

        (*aptr2).use();
        aptr2->use();

        assert(aptr1.get() == nullptr);
    }

    SECTION("can be returned from function")
    {
        auto aptr = create_gadget();
        create_gadget();

        use(aptr.get());
    }

    SECTION("can be passed as argument - tricky!!!")
    {
        use(create_gadget());  // ok

        auto aptr = create_gadget();
        use(aptr);  // problem

        //aptr->use(); // segfault
    }
}

namespace Modern
{
    // source - factory
    unique_ptr<Gadget> create_gadget()
    {
        static int counter = 100;
        return make_unique<Gadget>(++counter);
    }


    // sink
    void use(unique_ptr<Gadget> ptr)
    {
        ptr->use();
    }
}

class Owner
{
    unique_ptr<Gadget> g_;
public:
    Owner(unique_ptr<Gadget> g) : g_{move(g)}
    {}

    void play()
    {
        g_->use();
    }
};

TEST_CASE("unique_ptr")
{
    unique_ptr<Gadget> ptr{ new Gadget(100) };

    ptr->use();
    (*ptr).use();

    SECTION("C++14 uses make_unique")
    {
        unique_ptr<Gadget> ptr2 = make_unique<Gadget>(200, "ipad");
    }

    SECTION("can be moved")
    {
        auto ptr2 = move(ptr);

        REQUIRE(ptr.get() == nullptr);

        ptr2->use();
    }

    SECTION("can be returned from function")
    {
        auto uptr = Modern::create_gadget();
        Modern::create_gadget();

        use(uptr.get());
    }

    SECTION("can be passed as argument")
    {
        Modern::use(Modern::create_gadget());  // ok

        auto uptr = Modern::create_gadget();
        Modern::use(move(uptr));  // problem
    }

    SECTION("can be stored in STL containers")
    {
        cout << "\n\n\n";

        vector<unique_ptr<Gadget>> gadgets;

        gadgets.push_back(make_unique<Gadget>(100, "ipod"));
        gadgets.push_back(make_unique<Gadget>(101, "ipad"));
        gadgets.push_back(Modern::create_gadget());
        gadgets.push_back(Modern::create_gadget());
        gadgets.push_back(move(ptr));

        gadgets[1]->use();

        gadgets.back() = make_unique<Gadget>(115, "mobile");

        for(const auto& g : gadgets)
            g->use();
    }
}

TEST_CASE("Owner tests")
{
    Owner o1{make_unique<SuperGadget>(665, "nokia3310")};

    o1.play();

    Owner o2 = move(o1);
    o2.play();
}

TEST_CASE("unique_ptr can manage arrays")
{
    unique_ptr<Gadget[]> gadgets{new Gadget[10]}; // C++11
    auto other_gadgets = make_unique<SuperGadget[]>(10); // C++14

    gadgets[0].use();
    other_gadgets[6].use();
}

void may_throw()
{
    // throw 13;
}

class FileCloser
{
public:
    void operator()(FILE* f)
    {
        fclose(f);
    }
};

void save_file(FILE* f)
{
    //...
}

TEST_CASE("unique_ptr supports custom deallocators")
{
    SECTION("legacy code")
    {
        FILE* f = fopen("data.txt", "w+");

        save_file(f);
        may_throw();

        fclose(f);
    }

    SECTION("modern code")
    {
        unique_ptr<FILE, FileCloser> f{fopen("data.txt", "w+")};

        save_file(f.get());
        may_throw();
    }

    SECTION("more modern code")
    {
        auto closer = [](FILE* f) { fclose(f); };
        unique_ptr<FILE, decltype(closer)> f{fopen("data.txt", "w+"), closer};

        save_file(f.get());
        may_throw();
    }
}

TEST_CASE("shared_ptr")
{
    auto sp1 = make_shared<Gadget>(1, "ipad");
    {
        auto sp2 = sp1;
        REQUIRE(sp1.use_count() == 2);
    }
    REQUIRE(sp1.use_count() == 1);
    REQUIRE(sp1.unique());

    SECTION("unique_ptr can be moved to shared_ptr")
    {
        cout << "\n\n";
        unique_ptr<Gadget> up = Modern::create_gadget();
        shared_ptr<Gadget> sp = move(up);

        sp->use();

        sp = Modern::create_gadget();
        sp->use();
    }

    SECTION("supports custom deallocator")
    {
        shared_ptr<FILE> text_file{fopen("data.txt", "w+"), [](FILE* f) { if (f) fclose(f); }};

        save_file(text_file.get());
        may_throw();
    }
}

TEST_CASE("weak_ptr")
{
    weak_ptr<Gadget> wp;
    auto sp1 = make_shared<Gadget>(1, "ipad");

    {
        auto sp2 = sp1;
        REQUIRE(sp1.use_count() == 2);

        wp = sp2;
    }

    SECTION("must converted to shared_ptr to use object")
    {
        SECTION("using shared_ptr constructor")
        {
            shared_ptr<Gadget> local_sp{wp};
            local_sp->use();
            local_sp.reset();

            SECTION("when object is deallocated constructor throws")
            {
                REQUIRE(sp1.use_count() == 1);
                sp1.reset();

                REQUIRE_THROWS_AS(shared_ptr<Gadget>{wp}, std::bad_weak_ptr);
            }
        }

        SECTION("using lock()")
        {
            shared_ptr<Gadget> local_sp = wp.lock();
            if (local_sp)
                local_sp->use();
        }
    }
}
















