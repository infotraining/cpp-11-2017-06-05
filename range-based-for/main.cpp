#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <vector>
#include <list>

using namespace std;

TEST_CASE("range-based-for")
{
    vector<int> vec = { 1, 2, 3, 4, 5, 6, 7 };

    SECTION("works with containers")
    {
        for(int x : vec)
        {
            cout << x << " ";
        }
        cout << endl;

        SECTION("is interpreted by compiler as")
        {
            for(auto it = vec.begin(); it != vec.end(); ++it)
            {
                int x = *it;

                cout << x << " ";
            }
        }
    }

    SECTION("works with auto declaration")
    {
        for(auto item : vec)
        {
            cout << item << " ";
        }
        cout << endl;

        SECTION("but we should avoid performance trap")
        {
            vector<string> words = { "one", "two", "three" };

            for(const auto& word : words)
            {
                cout << word << " ";
            }
            cout << endl;
        }
    }

    SECTION("works with native arrays")
    {
        int tab[] = { 1, 2, 3, 4, 5 };

        for(const auto& item : tab)
        {
            cout << item << " ";
        }
        cout << endl;

        SECTION("is interpreted as")
        {
            for(auto it = begin(tab); it != end(tab); ++it)
            {
                const auto& x = *it;

                cout << x << " ";
            }
        }
    }

    SECTION("works with initializer_list")
    {
        for(const auto& item : { "one", "two", "three" })
        {
            cout << item << " ";
        }
        cout << endl;
    }
}