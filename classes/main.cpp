#include <iostream>
#include <string>
#include <vector>
#include <type_traits>

using namespace std;

template <typename T, typename = typename enable_if<is_integral<T>::value>::type>
void integral_only(T x)
{
    cout << "integral_only(int: " << x << ")" << endl;
}

// all special functions defaulted by compiler
class AllDefault
{
public:
    string text = "text";
    int id{-1};
};

class NoCopyable
{
protected:
    NoCopyable() = default;
public:
    NoCopyable(const NoCopyable&) = delete;
    NoCopyable& operator=(const NoCopyable&) = delete;
};

class Drm : NoCopyable
{
    int id = -1;
    string title = "unknown";
public:
    Drm() = default;
    Drm(int id) : id{id}
    {}
};

class DelegateConstruct
{
public:
    DelegateConstruct(int i)
    {
        cout << "DelegateConstruct(int: " << i << ")" << endl;
    }

    DelegateConstruct(string txt) : DelegateConstruct{stoi(txt)}
    {
        throw runtime_error("Error#13");
        cout << "DelegateConstruct(string: " << txt << ")" << endl;
    }

    ~DelegateConstruct()
    {
        cout << "~DelegateConstruct()" << endl;
    }
};

int main() try
{
    integral_only(42);

    integral_only(42L);

    integral_only(true);

    //integral_only(3.14f);

    AllDefault a1;
    a1.text = "text";
    AllDefault a2 = a1; // cc
    a1.text = "changed";
    a2 = a1; // c=

    cout << "before move a2: " << a2.text << "\n";

    AllDefault a3 = move(a2);

    cout << "after move a2: " << a2.text << "\n";
    cout << "after move a3: " << a3.text << "\n";

    //NoCopyable nc1;
    //NoCopyable nc2 = nc1;

    Drm drm1;
    //Drm drm2 = drm1; // Drm is no-copyable

    DelegateConstruct dc{"10"};
}
catch(const exception& e)
{
    cout << "Caught an exception: " << e.what() << endl;
}
