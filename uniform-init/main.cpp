#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <map>
#include <vector>
#include <boost/type_index.hpp>

using namespace std;

struct Point
{
    int x, y;
};

short get_value()
{
    return 13;
}

struct Vector2D
{
    int x, y;

    Vector2D(int x = 0, int y = 0) : x{x}, y{y}
    {
    }
};

template <typename F, typename Arg>
auto invoke(F f, const Arg &arg) //-> decltype(f(arg))
{
    return f(arg);
}

auto foo(int i)
{
    if (i % 2 == 0)
        return string("even");
    return "odd"s;
}

TEST_CASE("uniform initialization")
{
    SECTION("for simple types")
    {
        int x{10};
        REQUIRE(x == 10);

        int y = {10};
        REQUIRE(y == 10);

        int z{};
        REQUIRE(z == 0);

        bool flag{};
        REQUIRE(flag == false);

        int *ptr{}; // int* ptr = nullptr;
    }

    SECTION("for aggregates")
    {
        Point pt1{0, 1};
        bool expected = (pt1.x == 0) && (pt1.y == 1);
        REQUIRE(expected == true);
    }

    SECTION("narrowing conversion is an error")
    {
        short var{get_value()};

        REQUIRE(var == 13);
    }

    SECTION("for user class")
    {
        Vector2D vec1{1, 20};     // calling a constructor
        Vector2D vec2 = {50, 20}; // calling a constructor
        Vector2D vec3{};          // default constructed Vector2D
    }

    SECTION("for containers")
    {
        using namespace Catch::Matchers;

        vector<int> vec1 = {1, 2, 3, 4, 5, 6}; // vector<int>(initializer_list<int>)
        vector<int> vec2{1, 2, 3, 4, 5, 6};

        REQUIRE_THAT(vec1, Equals(vec2));
    }
}

TEST_CASE("initializer_list")
{
    using namespace Catch::Matchers;

    initializer_list<int> lst = {1, 2, 3};
    vector<int> vec1(lst);

    vector<int> vec2 = {1, 2, 3};
    REQUIRE_THAT(vec1, Equals(vec2));

    vec2.insert(vec2.end(), {4, 5, 6});
    REQUIRE_THAT(vec2, Equals(vector<int>{1, 2, 3, 4, 5, 6}));

    SECTION("auto deduces initializer list for brackets")
    {
        auto il = {1, 2, 3}; // initializer_list<int>
        static_assert(is_same<decltype(il), initializer_list<int>>::value, "Error");

        auto x{1}; // int
        static_assert(is_same<decltype(x), int>::value, "Error");
        auto y = {1}; // initializer_list; // since c++17 - int
        static_assert(is_same<decltype(y), initializer_list<int>>::value, "Error");
    }

    SECTION("initializer_list is prefered in constructors")
    {
        vector<int> vec1(5, 1);
        REQUIRE_THAT(vec1, Equals(vector<int>{1, 1, 1, 1, 1}));

        vector<int> vec2{10, 1};
        REQUIRE_THAT(vec2, Equals(vector<int>{10, 1}));

        vector<string> vec3{10};
        REQUIRE(vec3.size() == 10);
    }

    SECTION("initializer_lists with maps")
    {
        map<string, int> dict = {{"one", 1}, {"two", 2}, {"three", 3}};

        REQUIRE(dict["one"] == 1);

        using ReturnType = decltype(dict.cbegin());

        cout << "Type: " << typeid(ReturnType).name() << endl;
        cout << "Type: " << boost::typeindex::type_id<ReturnType>().pretty_name() << endl;

        decltype(dict) dict2;

        REQUIRE(dict2.size() == 0);
    }

    SECTION("invoke")
    {
        auto result = invoke(&foo, 10);

        static_assert(is_same<decltype(result), string>::value, "Error");
        REQUIRE(result == "even");
    }
}

template <typename Map, typename Key>
decltype(auto) find_by_key(Map &m, const Key &key)
{
    return m.at(key);
}

TEST_CASE("decltype(auto) case")
{
    map<string, int> dict = {{"one", 1}, {"two", 2}, {"three", 3}};

    auto &item = find_by_key(dict, "one");
    item = 11;

    REQUIRE(dict["one"] == 11);
}
