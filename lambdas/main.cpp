#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>

using namespace std;

class MagicLambda_236278462378
{
public:
    void operator()() const
    {
        cout << "lambda()" << endl;
    }
};

TEST_CASE("lambda")
{
    [](){[](){}();}(); // joke
    auto simplest_lambda = []{};

    auto l = [](){ cout << "lambda()" << endl; };
    l(); // call of closure object

    SECTION("deduces return type from single return in C++11")
    {
        auto multiply = [](int x, int y) { return x * y; };

        REQUIRE(multiply(3, 5) == 15);

        SECTION("require -> for many returns")
        {
            auto desc = [](int x) -> string {
                if (x % 2 == 0)
                    return "even"s;
                return "odd";
            };

            REQUIRE(desc(2) == "even");
            REQUIRE(desc(5) == "odd");
        }
    }

    SECTION("can be created and used in-place")
    {
        vector<shared_ptr<int>> vec = { make_shared<int>(8), make_shared<int>(5),
                                        make_shared<int>(2), make_shared<int>(9) };

        sort(vec.begin(), vec.end(),
             [](const shared_ptr<int>& a, const shared_ptr<int>& b) { return *a < *b; });

        sort(vec.begin(), vec.end(),
             [](const auto& a, const auto& b) { return *a < *b; });  // since C++14

        for_each(vec.begin(), vec.end(), [](const shared_ptr<int>& p) { cout << *p << " "; });
        cout << endl;
    }
}

class MagicLambda_2362784252436563
{
    const int x_;
public:
    MagicLambda_2362784252436563(int x) : x_{x}
    {}

    void operator()(int x) const
    {
        cout << "x from lambda: " << x_ << endl;
    }
};

TEST_CASE("captures and closures")
{
    // auto nc = [] { ++x; }; // compilation error - x is not captured

    SECTION("capture by value")
    {
        int x = 42;

        auto l = [x] { cout << "x from lambda: " << x << endl; return x; };

        x = 46;

        REQUIRE(l() == 42);
    }

    SECTION("capture by ref")
    {
        int x = 42;

        auto l = [&x] { ++x; };

        REQUIRE(x == 42);
        l();
        REQUIRE(x == 43);
    }

    SECTION("both by value & ref")
    {
        int counter = 0;
        int x = 42;

        auto l = [x, &counter] { counter++; return x; };
        x = 665;

        REQUIRE(l() == 42);
        REQUIRE(counter == 1);
    }
}

// C++14
auto create_generator(int seed)
{
    return [seed]() mutable { return ++seed; };
}

class MagicLambda_234234t5436345
{
    int seed_;
public:
    MagicLambda_234234t5436345() = delete;

    MagicLambda_234234t5436345(int seed) : seed_{seed}
    {}

    int operator()()
    {
        return ++seed_;
    }
};

TEST_CASE("mutable lambda")
{
    auto gen = create_generator(665);

    REQUIRE(gen() == 666);
    REQUIRE(gen() == 667);
}

TEST_CASE("storing lambda")
{
    SECTION("using auto")
    {
        auto l = [] { cout << "lambda\n"; };
        l();
    }

    SECTION("using ptr to function - when lambda has empty capture list")
    {
        void (*ptr_fun)() = [] { cout << "lambda\n"; };
        ptr_fun();
    }

    SECTION("using std::function")
    {
        function <void(int)> f = [](int x) { cout << "lambda(" << x << ")\n"; };
        f(44);
    }
}

void foo(int x)
{
    cout << "foo(" << x << ")\n";
}

struct Foo
{
    void operator()(int x)
    {
        cout << "Foo::operator()(" << x << ")\n";
    }
};

TEST_CASE("std::function")
{
    function<void(int)> f;

    SECTION("can be checked if empty")
    {
        if (f)
            f(13);
    }

    SECTION("call of empty function throws")
    {
        REQUIRE_THROWS_AS(f(13), bad_function_call);
    }

    SECTION("can store function pointer")
    {
        f = &foo;
        f(42);
    }

    SECTION("can store function object")
    {
        f = Foo{};
        f(43);
    }

    SECTION("can store lambda")
    {
        f = [](int x) { cout << "lambda(" << x << ")\n"; };
        f(44);
    }
}













