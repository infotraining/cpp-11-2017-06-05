#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>

using namespace std;

template <typename T, typename... Args>
unique_ptr<T> my_make_unique(Args&&... args)
{
    return unique_ptr<T>{ new T(forward<Args>(args)...) };
}

TEST_CASE("my_make_unique")
{
    std::unique_ptr<vector<string>> vec = my_make_unique<vector<string>>(10, "test");

    auto items = { string("one"), string("two"), string("three") };
    std::unique_ptr<vector<string>> vec2 = my_make_unique<vector<string>>(items);
}

/////////////////////////////////////////////////////////
/// Head-Tail idiom
///

template <typename... Ts>
struct Count;

template <typename Head, typename... Tail>
struct Count<Head, Tail...>
{
    static const size_t value = 1 + Count<Tail...>::value;
};

template <>
struct Count<>
{
    static const size_t value = 0;
};

template<typename... Ts>
struct TestCount
{
    static_assert(sizeof...(Ts) == Count<Ts...>::value, "Error");
};

TEST_CASE("count items")
{
    static_assert(Count<int, int, double, char>::value == 4, "Error");


    TestCount<int, int, int>{};
}


/////////////////////////////////////////////////////
/// Head-Tail for function templates
///
void print()
{
    cout << endl;
}

template <typename Head, typename... Tail>
void print(Head first, Tail... rest)
{
    cout << first << " ";
    print(rest...);
}

TEST_CASE("print")
{
    print(1, 3.14, "test");
}

constexpr size_t power2(size_t x)
{
    size_t result = 1;
    for(size_t i = 0; i < x; ++i)
        result *= 2;

    return result;
}

TEST_CASE("constexpr")
{
    array<int, power2(8)> a = {};

    static_assert(a.size() == 256, "Error");
}
