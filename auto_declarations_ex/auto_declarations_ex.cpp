#include <iostream>
#include <type_traits>
#include <vector>

using namespace std;

int x;
const int cx = 1;
int tab[10] = {};
vector<int> vec = {1, 2, 3};
const vector<int> cvec = {1, 2, 3};

using TODO = void;

int main()
{
    auto a1 = x;
    static_assert(is_same<int, decltype(a1)>::value, "Error");

    auto a2 = &x;
    static_assert(is_same<int*, decltype(a2)>::value, "Error");

    auto* a3 = &x;
    static_assert(is_same<int*, decltype(a3)>::value, "Error");

    auto a4 = &cx;
    static_assert(is_same<const int*, decltype(a4)>::value, "Error");

    auto* a5 = &cx;
    static_assert(is_same<const int*, decltype(a5)>::value, "Error");

    int& rx = x;
    const int& crx = x;

    const auto& a6 = rx;
    static_assert(is_same<const int&, decltype(a6)>::value, "Error");

    auto a7 = crx;
    static_assert(is_same<int, decltype(a7)>::value, "Error");

    auto& a8 = crx;
    static_assert(is_same<const int&, decltype(a8)>::value, "Error");

    auto a9 = tab;
    static_assert(is_same<int*, decltype(a9)>::value, "Error");

    auto& a10 = tab;
    static_assert(is_same<int(&)[10], decltype(a10)>::value, "Error");

    auto a11 = vec.begin();
    static_assert(is_same<vector<int>::iterator, decltype(a11)>::value, "Error");

    auto a12 = vec.cbegin();
    static_assert(is_same<vector<int>::const_iterator, decltype(a12)>::value, "Error");

    auto a13 = vec.back();
    static_assert(is_same<int, decltype(a13)>::value, "Error");

    auto& a14 = cvec.back();
    static_assert(is_same<const int&, decltype(a14)>::value, "Error");

    const auto a15 = vec[0];
    static_assert(is_same<const int, decltype(a15)>::value, "Error");

    auto&& a16 = 7.5;
    static_assert(is_same<double&&, decltype(a16)>::value, "Error");

    auto&& a17 = crx;
    static_assert(is_same<const int&, decltype(a17)>::value, "Error");

    auto&& a18 = vec.front();
    static_assert(is_same<int&, decltype(a18)>::value, "Error");
}
