#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <type_traits>
#include <iostream>
#include <vector>
#include <list>

using namespace std;

TEST_CASE("auto")
{   
    int x = 10;
    const int cx = 10;

    SECTION("can be used with variable declarations")
    {
        auto ax1(10); // int
        auto ax2 = 10L; // long
        auto txt1 = "text"; // const char*
        auto txt2 = "text"s; // string
    }

    SECTION("with pointers")
    {
        auto ptr1 = &x;  // int*
        static_assert(is_same<decltype(ptr1), int*>::value, "types are not the same");

        auto* ptr2 = &x;
        static_assert(is_same<decltype(ptr2), int*>::value, "types are not the same");

        SECTION("with const modifier")
        {
            auto const const_ptr = &x;
            static_assert(is_same<decltype(const_ptr), int* const>::value, "types are not the same");

            auto ptr_to_const = &cx;
            static_assert(is_same<decltype(ptr_to_const), const int*>::value, "types are not the same");

            auto const const_ptr_to_const = &cx;
            static_assert(is_same<decltype(const_ptr_to_const), const int* const>::value, "types are not the same");
        }
    }

    SECTION("with references")
    {
        auto& ref_x = x;
        static_assert(is_same<decltype(ref_x), int&>::value, "types are not the same");

        const auto& cref_x = x;
        static_assert(is_same<decltype(cref_x), const int&>::value, "types are not the same");

        auto& ref_cx = cx;
        static_assert(is_same<decltype(ref_cx), const int&>::value, "types are not the same");
    }
}

TEST_CASE("auto with iterators")
{
    vector<int> vec = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    for(auto it = vec.cbegin(); it != vec.cend(); ++it)
    {
        cout << *it << " ";
    }
    cout << endl;
}

TEST_CASE("auto type deduction mechanism")
{
    int x = 10;
    const int cx = 20;
    int& ref_x = x;
    const int& cref_x = cx;
    int tab[10] = {};

    SECTION("case 1")
    {
        auto& nrx1 = cx; // const int&
        auto& nrx2 = cref_x; // const int&

        auto& ntab = tab; // int(&)[10]
    }

    SECTION("case 3")
    {
        auto nx1 = ref_x; // int, reference is stripped away
        auto nx2 = cref_x; // int, ref & const are stripped away

        auto ntab = tab; // int* - decay to pointer
    }
}