#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <thread>
#include <iostream>

using namespace std;

TEST_CASE("raw-strings")
{
    string raw_str = R"(c:\nasza\teczka)";

    REQUIRE(raw_str == "c:\\nasza\\teczka");
}

TEST_CASE("raw-string can be used with many lines")
{
    string raw_str = R"(Line1
Line2
Line3)";

    REQUIRE(raw_str == "Line1\nLine2\nLine3");
}

TEST_CASE("raw-string can have custom delim")
{
    string raw_str = R"radmor(c:\text)radmor";

    REQUIRE(raw_str == "c:\\text");
}

TEST_CASE("c++14 has new string literal")
{
    auto str1 = "text"s;
    string str2 = string("text");
}
