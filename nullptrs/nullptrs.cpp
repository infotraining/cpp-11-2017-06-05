#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>

using namespace std;

int* foo()
{
    return nullptr;
}

TEST_CASE("nullptr is better NULL")
{
    int* ptr = nullptr;

    REQUIRE(ptr == 0);
    REQUIRE(ptr == nullptr);
}

TEST_CASE("nullptr in if")
{
    int* ptr = foo();

    if (ptr)
    {
        cout << "not nullptr" << endl;
    }

    if (ptr == nullptr)
    {
        cout << "not nullptr" << endl;
    }
}

TEST_CASE("nullptr has it's own type")
{
    try
    {
        int* sth = nullptr;
        throw nullptr;
    }
    catch (nullptr_t)
    {
        cout << "caught nullptr_t" << endl;
    }
    catch(int* ptr)
    {
        cout << "caught int*" << endl;
    }
}
