#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <type_traits>

using namespace std;

enum class Coffee
{
    espresso = 1,
    cappuccino,
    latte
};

enum class Engine : unsigned char
{
    diesel,
    tdi,
    gasoline
};

ostream& operator<<(ostream& out, const Engine& e)
{
    out << "Engine(" << static_cast<int>(e) << ")";

    return out;
}

TEST_CASE("scoped-enums")
{
    SECTION("implicit conversion from/to an int is not allowed")
    {
        Coffee c;

        c = Coffee::espresso;

        REQUIRE(c == Coffee::espresso);

        SECTION("comparing with an int requires cast")
        {
            REQUIRE(static_cast<int>(c) == 1);
        }

        SECTION("creating an enum from an int requires cast")
        {
            c = static_cast<Coffee>(2);

            REQUIRE(c == Coffee::cappuccino);
        }
    }

    SECTION("comparing and assingnment from other enum is not allowed")
    {
        Coffee c = Coffee::latte;

        //REQUIRE(c == Engine::tdi);  // compilation error
    }

    SECTION("writing enum to ostream")
    {
        Engine e = Engine::tdi;

        cout << e << endl;
    }
}

TEST_CASE("underlying type")
{
    using EngineIntegralType = underlying_type<Engine>::type;

    static_assert(is_same<EngineIntegralType, unsigned char>::value, "Error");
}