#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>

using namespace std;

string full_name(const string& fn, const string& ln)
{
    return fn + " " + ln;
}

class Person
{
    string name_ = "unknown";
public:
    Person() = default;

    Person(const string& name) : name_{name}
    {}

    string name() const
    {
        return name_;
    }

    void set_name(const string& name)
    {
        cout << "Person::set_name(const string& " << name << ")" << endl;
        name_ = name;
    }

    void set_name(string&& name)
    {
        cout << "Person::set_name(string&& " << name << ")" << endl;
        name_ = move(name); // optimized impl
    }
};

namespace RR
{
    inline namespace Lame
    {
        class Radmor
        {
            size_t size_{};
            Person* stuff_ = nullptr;
            string payroll_category_ = "basic";
        public:
            using iterator = Person*;
            using const_iterator = const Person*;

            Radmor() = default;

            Radmor(size_t size, const string& payroll_cat)
                : size_{size}, stuff_{new Person[size]}, payroll_category_{payroll_cat}
            {
                cout << "Radmor(size_t: " << size << ")" << endl;
            }

            Radmor(initializer_list<Person> stuff) : size_{stuff.size()}, stuff_{new Person[stuff.size()] }
            {
                cout << "Radmor(il: " << stuff.size() << ")" << endl;

                copy(stuff.begin(), stuff.end(), stuff_);
            }

            // copy-constructor
            Radmor(const Radmor& src) : size_{src.size_}, stuff_{new Person[src.size_]}, payroll_category_{src.payroll_category_}
            {
                cout << "Radmor(const Radmor&)" << endl;
                copy(src.begin(), src.end(), stuff_);
            }

            // copy-assignment
            Radmor& operator=(const Radmor& src)
            {
                if (this != &src)
                {
                    cout << "Radmor::operator=(const Radmor&)" << endl;

                    payroll_category_ = src.payroll_category_;
                    Person* temp = new Person[src.size_];
                    copy(src.begin(), src.end(), temp);
                    size_ = src.size_;
                    delete[] stuff_;
                    stuff_ = temp;
                }

                return *this;
            }

            // move-constructor
            Radmor(Radmor&& src) : size_{move(src.size_)}, stuff_{move(src.stuff_)}, payroll_category_{move(src.payroll_category_)}
            {
                cout << "Radmor(Radmor&&)" << endl;
                src.stuff_ = nullptr;
            }

            // move-assignment
            Radmor& operator=(Radmor&& src)
            {
                if (this != &src)
                {
                    cout << "Radmor::operator=(Radmor&&)" << endl;

                    size_ = move(src.size_);
                    stuff_ = move(src.stuff_);
                    payroll_category_ = move(src.payroll_category_);

                    src.stuff_ = nullptr;
                }

                return *this;
            }


            Person& operator[](size_t index)
            {
                return stuff_[index];
            }

            const Person& operator[](size_t index) const
            {
                return stuff_[index];
            }

            ~Radmor()
            {
                delete[] stuff_;
            }

            iterator begin()
            {
                return stuff_;
            }

            iterator end()
            {
                return stuff_ + size_;
            }

            const_iterator begin() const
            {
                return stuff_;
            }

            const_iterator end() const
            {
                return stuff_ + size_;
            }
        };
    }

    namespace Modern
    {
        class Radmor
        {
            vector<Person> stuff_;
            string payroll_category_ = "basic";
        public:
            using iterator = vector<Person>::iterator;
            using const_iterator = vector<Person>::const_iterator;

            Radmor() = default;

            Radmor(size_t size, const string& payroll_cat)
                : stuff_(size), payroll_category_{payroll_cat}
            {
                cout << "Radmor(size_t: " << size << ")" << endl;
            }

            Radmor(initializer_list<Person> stuff) : stuff_(stuff)
            {
                cout << "Radmor(il: " << stuff.size() << ")" << endl;
            }

            Person& operator[](size_t index)
            {
                return stuff_[index];
            }

            const Person& operator[](size_t index) const
            {
                return stuff_[index];
            }

            iterator begin()
            {
                return stuff_.begin();
            }

            iterator end()
            {
                return stuff_.end();
            }

            const_iterator begin() const
            {
                return stuff_.begin();
            }

            const_iterator end() const
            {
                return stuff_.end();
            }
        };
    }
}

TEST_CASE("refence binding")
{
    string first_name = "J  an";

    SECTION("C++98")
    {
        string& ref_fn = first_name; // l-value can be bound to l-value ref

        const string& name = full_name(first_name, "Kowalski"); // r-value can be bound to const l-value ref
    }

    SECTION("C++11")
    {
        string&& rvalue_ref_fn = full_name(first_name, "Kowalski"); // r-value can be bound to r-value ref
        rvalue_ref_fn.assign("");

        //string&& rvalue_ref_name = first_name; // compile error - binding l-value to r-value ref is not allowed
    }

    SECTION("using r-value ref")
    {
        Person p;

        SECTION("set by copy")
        {
            p.set_name(first_name);

            first_name = "Adam";
        }

        SECTION("set by move")
        {
            p.set_name(full_name(first_name, "Kowalski"));

            p.set_name("Adam Nowak");

            p.set_name(move(first_name));
        }
    }
}

TEST_CASE("Radmor")
{
    using namespace RR;

    SECTION("can be initialized with a list")
    {
        Radmor radmor = { Person{"Jan"}, Person{"Adam"}, Person{"Jerzy"}, Person{"Zenon"} };

        radmor[0].set_name("Ola");

        for(const auto& person : radmor)
        {
            cout << person.name() << " ";
        }
        cout << endl;
    }

    SECTION("can be copied")
    {
        Radmor radmor = { Person{"Jan"}, Person{"Adam"}, Person{"Jerzy"}, Person{"Zenon"} };

        Radmor backup = radmor;

        for(const auto& person : backup)
            cout << person.name() << " ";
        cout << endl;

        Radmor backup2;
        backup2 = backup;

        for(const auto& person : backup2)
            cout << person.name() << " ";
        cout << endl;
    }

    SECTION("can be moved")
    {
        Radmor radmor = { Person{"Jan"}, Person{"Adam"}, Person{"Jerzy"}, Person{"Zenon"} };

        Radmor moved_radmor = move(radmor);

        for(const auto& person : moved_radmor)
            cout << person.name() << " ";
        cout << endl;

        radmor = move(moved_radmor);

        for(const auto& person : radmor)
            cout << person.name() << " ";
        cout << endl;

        cout << "\n\n";

        vector<Radmor> radmor_group;
        //radmor_group.reserve(10);

        cout << "------------------- push_back" << endl;
        radmor_group.push_back(move(radmor));
        for(int i = 1; i <= 10; ++i)
        {
            cout << "------------------- push_back" << endl;
            radmor_group.push_back(Radmor{Person("Anna"), Person("Kazimierz")});
        }
    }
}

struct WB
{
    RR::Radmor radmor_;
};

TEST_CASE("WB tests")
{
    using namespace RR;

    cout << "\n\nWB tests:\n";

    WB wb;
    wb.radmor_ = Radmor{ Person{"Jan"}, Person{"Adam"}, Person{"Jerzy"}, Person{"Zenon"} };

    WB moved_wb = move(wb);

    vector<WB> wb_group;

    wb_group.push_back(move(moved_wb));
    wb_group.push_back(WB{});
    wb_group.push_back(WB{});
}
