#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <tuple>

using namespace std;

struct T4671245
{
    int a;
    double b;
    string c;
};

TEST_CASE("tuple")
{
    SECTION("creating tuples")
    {
        tuple<int, double, string> t1(99, 6.55, "abc");
        tuple<int, double, string> t2(1, 3.14, "text");

        //t1 = make_tuple(2, 5.44, "txt"s);

        REQUIRE(get<0>(t2) == 1);
        REQUIRE(get<1>(t2) == 3.14);
        REQUIRE(get<2>(t2) == "text");

        SECTION("can be moved")
        {
            t1 = move(t2);

            REQUIRE(get<0>(t1) == 1);
            REQUIRE(get<1>(t1) == 3.14);
            REQUIRE(get<2>(t1) == "text");
        }
    }
}

tuple<int, int, double> calc_stats(const vector<int>& data)
{
    vector<int>::const_iterator min_it, max_it;
    tie(min_it, max_it) = minmax_element(data.begin(), data.end());

    double avg = accumulate(data.begin(), data.end(), 0.0) / data.size();

    return make_tuple(*min_it, *max_it, avg);
}

TEST_CASE("calc_stats tests")
{
    vector<int> data = { 8, 534, 234, 1, 34, 453, 9 };

    auto stats = calc_stats(data);

    REQUIRE(get<0>(stats) == 1);
    REQUIRE(get<1>(stats) == 534);

    SECTION("with ref tuple")
    {
        int min, max;
        double avg;

        tuple<int&, int&, double&> stats{min, max, avg};

        get<0>(stats) = 10;
        REQUIRE(min == 10);

        stats = calc_stats(data);
        REQUIRE(min == 1);
        REQUIRE(max == 534);
    }

    SECTION("with tie")
    {
        int min, max;
        double avg;

        tie(min, max, avg) = calc_stats(data);

        REQUIRE(min == 1);
        REQUIRE(max == 534);
    }

    SECTION("with tie with ignore")
    {
        int min, max;

        tie(min, max, ignore) = calc_stats(data);

        REQUIRE(min == 1);
        REQUIRE(max == 534);
    }

//    SECTION("C++17 - structured bindings")
//    {
//        auto[min, max, avg] = calc_stats(data);

//        REQUIRE(min == 1);
//        REQUIRE(max == 534);
//    }
}

class Gadget
{
    int id_ = -1;
    string name_ = "unknown";
private:
    auto tied() const
    {
        return tie(id_, name_);
    }
public:
    Gadget() = default;

    Gadget(int id) : id_{id}
    {
        cout << "Gagdet(" << id_ << ")" << endl;
    }

    Gadget(int id, const string& name) : id_{id}, name_{name}
    {
        cout << "Gagdet(" << id_ << ", " << name_ << ")" << endl;
    }

    virtual ~Gadget()
    {
        cout << "~Gadget(" << id_ << ")" << endl;
    }

    int id() const
    {
        return id_;
    }

    virtual void use()
    {
        cout << "Using Gadget(" << id_ << ")\n";
    }

    bool operator ==(const Gadget& other) const
    {
        return tied() == other.tied();
    }

    bool operator !=(const Gadget& other) const
    {
        return tied() != other.tied();
    }

    bool operator <(const Gadget& other) const
    {
        return this->tied() < other.tied();
    }
};

TEST_CASE("compare objects in C++11")
{
    Gadget g1{1, "ipod"};
    Gadget g2{1, "ipad"};

    REQUIRE(g1 != g2);
    REQUIRE(g1 < g2);
}













