#include <functional>
#include <iostream>
#include <set>

template <typename T, typename Comparer = std::less<T>>
struct IndexableSet : std::set<T, Comparer>
{
    using SetType = std::set<T, Comparer>;
    using size_type = typename SetType::size_type;

    using std::set<T, Comparer>::set; // inherit constructors of std::set

    //IndexableSet(std::initializer_list<T>) = delete;

    const T& operator[](size_type index) const
    {
        return this->at(index);
    }

    const T& at(size_type index) const
    {
        if (index >= SetType::size())
            throw std::out_of_range{"indexableSet::operator[] out of range"};
        auto iter = SetType::begin();
        std::advance(iter, index);
        return *iter;
    }
};

int main()
{
    using namespace std;

    IndexableSet<int> indexed_set = {5, 1, 8, 4, 8, 12, -2, 7};

    cout << "size: " << indexed_set.size() << endl;

    cout << "3rd item: " << indexed_set[2] << endl;
}
