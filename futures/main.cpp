#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <random>
#include <future>

using namespace std;

int square(int x)
{
    cout << "Start square(" << x << ")" << endl;

    random_device rd;
    mt19937_64 rnd_gen(rd());
    uniform_int_distribution<int> distr(200, 3000);

    auto interval = distr(rnd_gen);

    this_thread::sleep_for(chrono::milliseconds(interval));

    if (x == 8)
        throw runtime_error("Error#8");

    return x * x;
}

int main()
{
    future<int> f1 = async(launch::async, &square, 9);
    auto f2 = async(launch::async, [] { return square(7);});

    //...

    cout << "f1.get() = " << f1.get() << endl;
    cout << "f2.get() = " << f2.get() << endl;


    vector<future<int>> fresults;

    for(int i = 1; i < 20; ++i)
    {
        fresults.push_back(async(launch::async, [i] { return square(i); }));
    }

    for(auto& f : fresults)
    {
        try
        {
            auto result = f.get();
            cout << "result: " << result << endl;
        }
        catch(const runtime_error& e)
        {
            cout << e.what() << endl;
        }
    }

}
